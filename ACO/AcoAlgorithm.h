#pragma once
#include <stdio.h>
#include <cstdlib>
#include <vector>

class AcoAlgorithm
{
	const static int NUMBER_OF_CITIES = 30;
	const static int MAP_WIDTH = 1100;
	const static int MAP_HEIGHT = 700;
	const static int NUMBER_OF_ANTS = 400;
	
	int acoIteration;

	double alpha;
	double beta;
	double pheromoneEvaporation;
	double q;

	double cityX[NUMBER_OF_CITIES];
	double cityY[NUMBER_OF_CITIES];
	double pheromoneMatrix[NUMBER_OF_CITIES][NUMBER_OF_CITIES];
	double distanceMatrix[NUMBER_OF_CITIES][NUMBER_OF_CITIES];
	double invertedDistanceMatrix[NUMBER_OF_CITIES][NUMBER_OF_CITIES];
	
	int antPaths[NUMBER_OF_ANTS][NUMBER_OF_CITIES];
	int antPathDistances[NUMBER_OF_ANTS];
	double pathProbability[NUMBER_OF_CITIES];
	double deltaMatrix[NUMBER_OF_CITIES][NUMBER_OF_CITIES];

	int bestPath[NUMBER_OF_CITIES];
	double bestDistance;
	double bestDistanceAtStart;
	
	void InitCities(void);

	void GeneratePaths(void);
	void SelectBestPath(void);
	void UpdatePheromones(void);

	double CalculateTotalDistance(int path[]);
	double EuclideanDistance(int city1, int city2);

public:
	AcoAlgorithm(void);
	~AcoAlgorithm(void);

	void Update(void);
	void Reset(void);

	// getters
	int GetCityCount(void);
	int GetCityX(int cityIndex);
	int GetCityY(int cityIndex);
	int GetIteration();
	double GetBestDistance(void);
	double GetBestDistanceAtStart(void);
	int* GetBestPath(void);
	int GetNumberOfCities(void);
};

