#pragma once
#include <SDL.h>
#include <SDL_opengl.h>

#define For(i,N)     for (int (i) = 0; (i) < (N); (i)++)

class TextPrinter
{
	char (*mMtxFont)[7][5];
	void InitMtxFont();

public:
	TextPrinter(void);
	~TextPrinter(void);

	void Draw_MtxText(float x, float y, const char *fmt,...);
	void Draw_MtxFont(float x, float y, Uint8 c);
};

