#include "AcoGui.h"

AcoGui::AcoGui(void)
{
	srand((int)time(NULL));
	tp = new TextPrinter();
	aco = new AcoAlgorithm();
}

AcoGui::~AcoGui(void)
{
	delete tp;
	delete aco;
}

void AcoGui::Update(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	DrawCities();
	DrawStatus();
	DrawBestPath();

	for(int i = 0; i < ITERATIONS_PER_FRAME; i++) aco->Update();
}

void AcoGui::Reset(void)
{
	aco->Reset();
}

void AcoGui::DrawStatus(void)
{
	int iteration = aco->GetIteration();
	double bestDistance = aco->GetBestDistance();
	double bestDistanceAtStart = aco->GetBestDistanceAtStart();

	glColor3ub(255,255,255);
	
	tp->Draw_MtxText(15, 35, "ITERATION %5d - START DIST %5.1f - BEST DIST %5.1f - DIFF %3.1f/", 
			iteration, bestDistanceAtStart, bestDistance, (bestDistance / bestDistanceAtStart)*100);

	glColor3ub(255,192,192);
	tp->Draw_MtxText(15, 10, "PRESS SPACE OR LEFT MOUSE BUTTON TO RESTART ALGORITHM");
}

void AcoGui::DrawCities(void)
{
	int cityCount = aco->GetCityCount();

	// background
	glColor3ub(0,0,0);
	glBegin(GL_QUADS);
	glVertex2f(MAP_X, MAP_Y); 
	glVertex2f(MAP_X+MAP_WIDTH, MAP_Y); 
	glVertex2f(MAP_X+MAP_WIDTH, MAP_HEIGHT+MAP_Y);
	glVertex2f(MAP_X, MAP_HEIGHT+MAP_Y);
	glEnd();

	for(int i = 0; i < cityCount; i++)
	{
		glPointSize((float)CITY_SIZE);
		glColor3ub(255,0,0);
		glBegin(GL_POINTS);
		glVertex2f(aco->GetCityX(i) + MAP_X, aco->GetCityY(i) + MAP_Y);
		glEnd();
	}
}

void AcoGui::DrawBestPath(void)
{
	int *path = aco->GetBestPath();

	glLineWidth(2);
	glColor3ub(255,255,255);
	glBegin(GL_LINE_STRIP);

	for(int i = 0; i < aco->GetNumberOfCities(); i++)
	{
		glVertex2f(aco->GetCityX(path[i])+MAP_X, aco->GetCityY(path[i])+MAP_Y);
	}
	glVertex2f(aco->GetCityX(path[0])+MAP_X, aco->GetCityY(path[0])+MAP_Y);
	glEnd();	
}