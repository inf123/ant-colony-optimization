#pragma once
#include <time.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include "AcoAlgorithm.h"
#include "TextPrinter.h"

class AcoGui
{
	AcoAlgorithm *aco;
	TextPrinter *tp;

	void DrawStatus(void);
	void DrawCities(void);
	void DrawBestPath(void);

public:
	AcoGui(void);
	~AcoGui(void);
	void Update(void);
	void Reset(void);

	const static int ITERATIONS_PER_FRAME = 1;

	const static int NUMBER_OF_CITIES = 10;
	const static int SCREEN_WIDTH = 1130;
	const static int SCREEN_HEIGHT = 775;
	const static int CITY_SIZE = 10;

	const static int MAP_X = 15;
	const static int MAP_Y = 60;
	const static int MAP_WIDTH = 1100;
	const static int MAP_HEIGHT = 700;
};

