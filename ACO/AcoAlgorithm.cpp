#include "AcoAlgorithm.h"

AcoAlgorithm::AcoAlgorithm()
{
	Reset();
}

AcoAlgorithm::~AcoAlgorithm(void) { }

// Reset & init algorithm
void AcoAlgorithm::Reset(void)
{
	// set algorithm constants
	alpha = 1.5;
	beta = 0.8;
	pheromoneEvaporation = 0.4;
	q = 10.0;

	bestDistance = 0.0;
	acoIteration = 0;

	InitCities();

	// reset pheromone values and calculate distance + inverted distance between all cities
	for(int i = 0; i < NUMBER_OF_CITIES; i++)
	{
		for(int j = 0; j < NUMBER_OF_CITIES; j++)
		{
			double dist = 0.0;
			double invertedDist = 0.0;

			if(i != j) 
			{
				dist = EuclideanDistance(i, j);
				invertedDist = 1 / dist;
			}

			distanceMatrix[i][j] = dist;
			invertedDistanceMatrix[i][j] = invertedDist;
			pheromoneMatrix[i][j] = 1.0;

			//printf("[%d,%d] D: %8.2f  iD: %7.6f  ", i ,j, dist, invertedDist);
		}
	}
	
	Update(); // run once to get starting distance

	bestDistanceAtStart = bestDistance;
}

// Update main algorithm
void AcoAlgorithm::Update(void)
{
	GeneratePaths();
	SelectBestPath();
	UpdatePheromones();

	acoIteration++;
}

// Generate a path for each ant
void AcoAlgorithm::GeneratePaths(void)
{
	bool visitedCities[NUMBER_OF_CITIES];

	for(int i = 0; i < NUMBER_OF_ANTS; i++)
	{	
		for(int j = 0; j < NUMBER_OF_CITIES; j++) visitedCities[j] = false;

		int startCity = rand() % NUMBER_OF_CITIES;  // select random start city
		antPaths[i][0] = startCity;
		visitedCities[startCity] = true;

		// select rest of the path for this ant
		for(int j = 1; j < NUMBER_OF_CITIES; j++)
		{
			for(int k = 0; k < NUMBER_OF_CITIES; k++) pathProbability[k] = 0.0;

			double probabilitySum = 0.0;

			// calculate probability for all unvisited cities
			for(int k = 0; k < NUMBER_OF_CITIES; k++)
			{
				if(visitedCities[k] == false)
				{
					// j-1 = last visited point on path, k = possible move
					int lastVisitedCity = antPaths[i][j-1];
					double p = (pow(pheromoneMatrix[lastVisitedCity][k], alpha)) * (pow(invertedDistanceMatrix[lastVisitedCity][k], beta));

					pathProbability[k] = p;
					probabilitySum += p;
				}
			}

			// select a random city to visit next
			double rnd = ((double)rand()/(double)RAND_MAX)*probabilitySum;
			double probCount = 0.0;
			int nextStep;

			for(int k = 0; k < NUMBER_OF_CITIES; k++)
			{
				if(visitedCities[k] == false)
				{
					nextStep = k;
					probCount += pathProbability[k];
				}
				if(probCount > rnd) break;
			}

			antPaths[i][j] = nextStep;
			visitedCities[nextStep] = true;
		}
	}
}

// Selects best path from current generation
void AcoAlgorithm::SelectBestPath(void)
{
	double bestDistanceFound = 0.0;
	int bestPathIndex = -1;

	// calculate the distance for every ant
	for(int i = 0; i < NUMBER_OF_ANTS; i++)
	{
		double dist = CalculateTotalDistance(antPaths[i]);
		antPathDistances[i] = dist;

		if(dist < bestDistanceFound || bestPathIndex == -1)
		{
			bestPathIndex = i;
			bestDistanceFound = dist;
		}
	}

	// Update best distance & path ever found.
	if(bestDistanceFound < bestDistance || bestDistance == 0.0)
	{
		bestDistance = bestDistanceFound;
		for(int i = 0; i < NUMBER_OF_CITIES; i++)
		{
			bestPath[i] = antPaths[bestPathIndex][i];
		}
	}
}

void AcoAlgorithm::UpdatePheromones(void)
{
	// reset delta matrix
	for(int i = 0; i < NUMBER_OF_CITIES; i++)
	{
		for(int j = 0; j < NUMBER_OF_CITIES; j++)
		{
			deltaMatrix[i][j] = 0.0;
		}
	}

	// Calculate delta for all trails
	for(int i = 0; i < NUMBER_OF_ANTS; i++)
	{
		double delta = q / antPathDistances[i];

		for(int j = 0; j < NUMBER_OF_CITIES-1; j++)
		{
			int cityIndex1 = antPaths[i][j];
			int cityIndex2 = antPaths[i][j+1];

			deltaMatrix[cityIndex1][cityIndex2] += delta;
			deltaMatrix[cityIndex2][cityIndex1] += delta;  // both directions
		}

		// also calculate delta for trail back to starting point
		int cityIndex1 = antPaths[i][NUMBER_OF_CITIES-1];
		int cityIndex2 = antPaths[i][0];

		deltaMatrix[cityIndex1][cityIndex2] += delta;
		deltaMatrix[cityIndex2][cityIndex1] += delta;  // both directions
	}

	// Update pheromone values
	for(int i = 0; i < NUMBER_OF_CITIES; i++)
	{
		for(int j = 0; j < NUMBER_OF_CITIES; j++)
		{
			pheromoneMatrix[i][j] = ((1 - pheromoneEvaporation) * pheromoneMatrix[i][j]) + deltaMatrix[i][j];
		}
	}

}

// Get total distance for a path
double AcoAlgorithm::CalculateTotalDistance(int path[])
{
	double totalDistance = 0.0;

	for(int i = 0; i < NUMBER_OF_CITIES - 1; i++)
	{
		totalDistance += distanceMatrix[path[i]][path[i+1]];
	}
	totalDistance += distanceMatrix[path[NUMBER_OF_CITIES-1]][path[0]];

	return totalDistance;
}

// Calculate Euclidean distance between city1 and city2
double AcoAlgorithm::EuclideanDistance(int city1, int city2)
{
	return sqrt( pow(cityX[city1]-cityX[city2], 2) + pow(cityY[city1]-cityY[city2], 2));
}

// Places cities randomly on map
void AcoAlgorithm::InitCities(void)
{
	for(int i = 0; i < NUMBER_OF_CITIES; i++)
	{
		cityX[i] = (double) (rand() % MAP_WIDTH);
		cityY[i] = (double) (rand() % MAP_HEIGHT);
		//printf("City ID: %3d - X: %10.2f - Y: %10.2f\n", i, cityX[i], cityY[i]);
	}
}

// Getters
int AcoAlgorithm::GetCityCount(void) {	return NUMBER_OF_CITIES; }
int AcoAlgorithm::GetCityX(int cityIndex) { return this->cityX[cityIndex]; }
int AcoAlgorithm::GetCityY(int cityIndex) { return this->cityY[cityIndex]; }
int AcoAlgorithm::GetIteration(void) { return acoIteration; }
double AcoAlgorithm::GetBestDistance(void) { return bestDistance; }
double AcoAlgorithm::GetBestDistanceAtStart(void) { return bestDistanceAtStart; }
int* AcoAlgorithm::GetBestPath(void) { return bestPath; }
int AcoAlgorithm::GetNumberOfCities(void) { return NUMBER_OF_CITIES; }
