#include "AcoGui.h"
#include <SDL.h>
#include <SDL_opengl.h>

SDL_Surface *screen;
AcoGui *acoGui;

void InitScreen()
{
	SDL_Init(SDL_INIT_VIDEO);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,  8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
	screen = SDL_SetVideoMode(acoGui->SCREEN_WIDTH, acoGui->SCREEN_HEIGHT, 32, SDL_OPENGL );
	SDL_WM_SetCaption("Ant Colony Optimization ", NULL);
	glClearColor(0.0f, 0.0f, 0.4f, 1.0f);

	glViewport(0,0, acoGui->SCREEN_WIDTH, acoGui->SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, acoGui->SCREEN_WIDTH, acoGui->SCREEN_HEIGHT, 0.0f, -1.0f, 1.0f);   // Create Ortho View (0,0 At Top Left)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void MainLoop()
{
	SDL_Event event;
	int key;
    bool done = false;

	while(!done)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			case SDL_MOUSEBUTTONDOWN:
				acoGui->Reset();
				break;
			case SDL_MOUSEBUTTONUP:
				break;
			case SDL_KEYDOWN:
				key = event.key.keysym.sym;
				if(key == SDLK_ESCAPE) done = true;
				else if(key == SDLK_SPACE) acoGui->Reset();
				break;
			case SDL_QUIT:
				done = true;
				break;
			}
		}

		acoGui->Update();
		SDL_GL_SwapBuffers();
		SDL_Delay(1);
	}
}

int main(int argc, char *argv[])
{
	InitScreen();

	acoGui = new AcoGui();

	MainLoop();

	SDL_Quit();
	return 0;
}